#!/usr/bin/env mksh
exec 2>>error.log
bdir=$(realpath .)
[[ ! -d "$bdir/db/update" ]] && mkdir -p "$bdir/db/update"
new_session() {
	[[ "$bdir" ]] && rm -rf "$bdir/tmp/" && mkdir -p "$bdir/tmp/vsu"
	touch "$bdir/tmp/hb_s" "$bdir/db/d_key"
	printf '%s\n' "$(tr -d '\n\| ' < "$bdir/id.json")"
}

resume_session() {
	session_id=$(jshon -Q -e d -e session_id -u < "$bdir/tmp/ready.json")
	printf '%s\n' "{\"op\":6,\"d\":{\"token\":\"$DTOKEN\",\"session_id\":\"$session_id\",\"seq\":$(cat "$bdir/tmp/seq_n")}}"
}

heartbeat() {
	while true; do
		sleep 10
		printf '%s\n' "{\"op\": 1,\"d\": $(cat "$bdir/db/d_key")}"
	done
}

check_websocket() {
	while true; do
		sleep 10
		if [[ "$(grep -w "$(cat "$bdir/websocket.pid")" <<< "$(ps aux)")" == "" ]]; then
			printf '%s\n' "$(date "+%F %R:%S") crashed" >> "$bdir/crash.log"
			kill -9 "$hb_pid"
			"$bdir/websocket.sh"
			exit 1
		fi
	done
}
heartbeat &
hb_pid=$!
check_websocket &
while read -r input; do
	op_code=$(jshon -Q -e op -u <<< "$input")
	case "$op_code" in
		9)
			new_session
		;;
		10)
			resume_session
			if [[ "$(jshon -Q -e d -e heartbeat_interval -u <<< "$input")" ]]; then
				hi=$(jshon -Q -e d -e heartbeat_interval -u <<< "$input")
				tail -c 4 <<< "$hi" > "$bdir/db/d_key"
				bc <<< "$(sed "s/../&./" <<< "$hi")*0.$((RANDOM%100))" > "$bdir/tmp/hb_s"
			fi
		;;
		11)
			printf '%s\n' "35" > "$bdir/tmp/hb_s"
		;;
	esac
	./reply_type.sh "$input" &
	jshon -Q -e s -u <<< "$input" > "$bdir/tmp/seq_n"
done
