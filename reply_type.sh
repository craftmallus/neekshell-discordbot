#!/usr/bin/env mksh
DTOKEN=$(cat token)
DAPI="https://discord.com/api/v9/"
PATH="$HOME/.local/bin:$PATH"
dheader="Authorization: Bot $DTOKEN"
bot="neekshellbot"
website="https://archneek.zapto.org/"
ver="1.0"
bdir=$(realpath .)
source d_method.sh
get_message_info() {
	message_id=$(jshon -Q -e d -e id -u <<< "$input")
	channel_id=$(jshon -Q -e d -e channel_id -u <<< "$input")
	guild_id=$(jshon -Q -e d -e guild_id -u <<< "$input")
	content=$(jshon -Q -e d -e content -u <<< "$input")
	user_id=$(jshon -Q -e d -e member -e user -e id -u <<< "$input")
	username=$(jshon -Q -e d -e member -e user -e username -u <<< "$input")
}
get_channel_info() {
	case "$1" in
		join)
			d_method get_channel "$channel_id"
			channel_name=$(jshon -Q -e name -u <<< "$dm_result")
			d_method get_guild "$guild_id"
			guild_name=$(jshon -Q -e name -u <<< "$dm_result")
		;;
		leave)
			guild_name=$(cut -f 2 -d : "$bdir/tmp/vsu/$guild_id/$user_id")
			channel_name=$(cut -f 4 -d : "$bdir/tmp/vsu/$guild_id/$user_id")
		;;
	esac
}
tg_auth() {
	cd /home/genteek/neekshell/
	source tg_method.sh
	TOKEN=$(cat token)
	TELEAPI="http://192.168.1.15:8081/bot${TOKEN}"
	cd - > /dev/null
}
tg_notify() {
	case "$1" in
		join)
			text_id=$(printf '%s' "discord ($guild_name): " \
				"$username joined $channel_name")
			mkdir -p "$bdir/tmp/vsu/$guild_id/"
			printf '%s' \
				"$guild_id:$guild_name:$channel_id:$channel_name" \
				> "$bdir/tmp/vsu/$guild_id/$user_id"
		;;
		leave)
			text_id=$(printf '%s' "discord ($guild_name): " \
				"$username left $channel_name")
			rm -f "$bdir/tmp/vsu/$guild_id/$user_id"
		;;
	esac
	chat_id=$2
	tg_auth
	tg_method send_message
}
tg_bridge() {
	if [[ ! -e "$bdir/tmp/vsu/$guild_id/$user_id" ]]; then
		local exit=false
	elif [[ "$channel_id" == "null" ]] && [[ -e "$bdir/tmp/vsu/$guild_id/$user_id" ]]; then
		channel_id=$(cut -f 3 -d : "$bdir/tmp/vsu/$guild_id/$user_id")
		local exit=true
	fi

	[[ ! "$exit" ]] && return

	up_dir="$bdir/db/update"
	for y in "$up_dir/channels:$channel_id" "$up_dir/users:$user_id"; do
		up_tg=($(grep -v "^#" "$(cut -f 1 -d : <<< "$y")"))
		check=$(cut -f 2 -d : <<< "$y")
		for x in $(seq 0 $((${#up_tg[@]}-1))); do
			if [[ "$(grep -w -- "$check\|all" <<< "$(cut -f 1 -d : <<< "${up_tg[$x]}")")" ]]; then
				local connected_chat=$(cut -f 2 -d : <<< "${up_tg[$x]}")
				break 2
			fi
		done
	done

	[[ ! "$connected_chat" ]] && return

	case "$exit" in
		false)
			action=join
		;;
		true)
			action=leave
		;;
	esac
	get_channel_info $action
	if [[ "$guild_name" ]] && [[ "$username" ]] && [[ "$channel_name" ]]; then
		tg_notify $action "$connected_chat"
	fi
}
input=$1
reply_type=$(jshon -Q -e t -u <<< "$input")
case "$reply_type" in
	READY)
		printf '%s\n' "$input" > "$bdir/tmp/ready.json"
	;;
	VOICE_STATE_UPDATE)
		get_message_info
		tg_bridge
	;;
	MESSAGE_CREATE)
		get_message_info
		case "$content" in
			"!ping")
				d_method create_message "$channel_id" pong reply
			;;
		esac
	;;
esac
