#!/usr/bin/env mksh

d_curl() {
	curl -A "$bot ($website, $ver)" \
		-H "$dheader" \
		-H 'Content-Type: application/json' \
		-s "${DAPI}${endpoint}" "$@"
}

d_method() {
	dm_result=$(case "$1" in
		get_channel)
			endpoint="channels/$2"
			d_curl
		;;
		get_guild)
			endpoint="guilds/$2"
			d_curl
		;;
		get_user)
			endpoint="users/$2"
			d_curl
		;;
		get_guild_channels)
			endpoint="guilds/$2/channels"
			d_curl
		;;
		create_message)
			endpoint="channels/$2/messages"
			if [[ ! "$fail_if_not_exists" ]]; then
				fail_if_not_exists=false
			fi
			case "$4" in
				reply)
					d_curl -d "$(jshon -Q \
						-n {} \
						-s "$3" -i content \
							-n {} \
							-s "$message_id" -i message_id \
							-s "$channel_id" -i channel_id \
							-s "$guild_id" -i guild_id \
							-s "$fail_if_not_exists" -i fail_if_not_exists \
						-i message_reference | tr -d '\n')"
			;;
				*)
					d_curl -d "$(jshon -Q -n {} -s "$3" -i content)"
			;;
			esac
		;;
	esac)
}
