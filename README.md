# neekshell-discordbot

Using websocat to connect to the discord gateway.<br />Example with current code: neekshellbot#2060

Utilities used:<br />
  - GNU coreutils & related<br />
  - cURL<br />
  - jshon (http://kmkeen.com/jshon/index.html)

Acting mainly as telegram notification bridge, using tg_method.sh from https://gitlab.com/craftmallus/neekshell-telegrambot
