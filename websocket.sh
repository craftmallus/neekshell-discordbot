#!/usr/bin/env bash

sleep 10
websocat --no-close -t -v "wss://gateway.discord.gg/?v=9&encoding=json" sh-c:'bash -c ./main.sh' > websocket.log 2>&1 &
websocket_pid=$!
printf '%s' "$websocket_pid" > websocket.pid
